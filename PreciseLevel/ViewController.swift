//
//  ViewController.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/03/14.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import UIKit
import CoreMotion
import CoreLocation

class ViewController: UIViewController, CLLocationManagerDelegate, UIDocumentInteractionControllerDelegate {
    // handles the sensors
    let motionManager = CMMotionManager()
    
    // handles the compass heading
    var locationManager: CLLocationManager!
    
    // IIR filtered angles
    var filteredPitch: Double = 0
    var filteredRoll: Double = 0
    var filteredYaw: Double = 0 // this is the one to deal with the compass
    var compositeLevelMagnitude: Double = 0
    var compositeLevelAngle: Double = 0
    var trueHeading: Double = 0
    var adjustedTrueHeading: Double = 0
    
    // Location related
    var locLatitude: Double = 0
    var locLongitude: Double = 0
    var locAccuracy: Double = 0
    var magHeading: Double = 0
    
    // Filter coefficient
    var alpha: Double = 0.4
    
    // the displayed angles, as strings
    var displayedSlopePercent: String!
    var displayedYaw: String!
    
    // these are for recording data
    var recordTimer: Timer!
    var recordTimestamp: Date!
    var samplesPitch: NSMutableArray = NSMutableArray.init()
    var samplesRoll: NSMutableArray = NSMutableArray.init()
    var samplesLevel: NSMutableArray = NSMutableArray.init()
    var samplesYaw: NSMutableArray = NSMutableArray.init()
    var isRecording: Bool = false
    let recordingInterval = 0.5
    var arraySamples:NSMutableArray = NSMutableArray.init()
    
    // selected scan
    var selectedScanType = -1
    var courseName:String!
    var holeName:String!
    
    // for recording
    var startTimeStamp: Date!
    
    // output file name forv the current scan
    var outFileName:String!
    
    // scan object that holds the above
    var currentScan:MappingScan!
    
    // data for all scans
    var scanLog:NSMutableArray = NSMutableArray.init()
    
    // scanning in progress
    var isScanning:Bool = false
    
    // level check in progress
    var isCheckingLevel:Bool = false
    
    // stats for level check
    var maxRotationDev: Double = 0
    var maxSlopeDev: Double = 0
    var rotatedSoFar: Double = 0
    var startingAngle: Double = 0
    var prevAngle: Double = 0
    
    // tolerances
    var rotationTol:Float = 10.0
    var slopeTol:Float = 1.5
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var labelPitch: UILabel!
    @IBOutlet weak var labelRoll: UILabel!
    @IBOutlet weak var labelYaw: UILabel!
    
    @IBOutlet weak var labelCompositeAngle: UILabel!
    @IBOutlet weak var imgViewCompositeAngle: UIImageView!
    @IBOutlet weak var labelGPSAccuracy: UILabel!
    
    @IBOutlet weak var viewScanOptions: UIView!
    @IBOutlet weak var viewScanInProgress: UIView!
    @IBOutlet weak var viewLevelCheck: UIView!
    @IBOutlet weak var viewButtonSet: UIView!
    @IBOutlet weak var viewLevelCheckControls: UIView!
    
    
    @IBOutlet weak var sliderRoll: UISlider!
    @IBOutlet weak var sliderPitch: UISlider!
    
    @IBOutlet weak var buttonRecord: UIButton!
    @IBOutlet weak var buttonShowSamples: UIButton!
    @IBOutlet weak var buttonSaveSamples: UIButton!
    
    @IBOutlet weak var buttonSettings: UIButton!
    @IBOutlet weak var buttonLog: UIButton!
    @IBOutlet weak var buttonUpload: UIButton!
    
    // scanner view
    @IBOutlet weak var labelScanType: UILabel!
    @IBOutlet weak var labelCourseHole: UILabel!
    @IBOutlet weak var labelTypeInProgress: UILabel!
    @IBOutlet weak var buttonSaveAndUpload: UIButton!
    
    // level check view
    @IBOutlet weak var labelDirectionDisplay: UILabel!
    @IBOutlet weak var labelPercentDisplay: UILabel!
    @IBOutlet weak var labelDirectionDeviation: UILabel!
    @IBOutlet weak var labelPercentDeviation: UILabel!
    @IBOutlet weak var labelRotationSoFar: UILabel!
    
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // first time: save tolerance settings
        let appSettings = UserDefaults.standard
        let usedBefore = appSettings.string(forKey: "usedBefore")
        if usedBefore == nil {
            appSettings.set("yes", forKey: "usedBefore")
            appSettings.set("10", forKey: "rotationTol")
            appSettings.set("1.5", forKey: "slopeTol")
            appSettings.synchronize()
        } else {
            // used before: read the settings
            rotationTol = Float(Double(appSettings.string(forKey: "rotationTol") ?? "10.0")! )
            slopeTol  = Float(Double(appSettings.string(forKey: "slopeTol") ?? "1.5")! )
        }
        
        // initialize the location manager
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest // request the best possible accuracy
        locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
            locationManager.startUpdatingHeading()
        }
        
        // not recording data yet
        isScanning = false
        
        // rotate the slider
        /*
        sliderPitch.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
        */
        
        // round corners
        let buttonSet1 = viewScanOptions.subviews.filter{$0 is UIButton}
        for button in buttonSet1 {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        let buttonSet2 = viewLevelCheck.subviews.filter{$0 is UIButton}
        for button in buttonSet2 {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        let buttonSet3 = viewScanInProgress.subviews.filter{$0 is UIButton}
        for button in buttonSet3 {
            button.layer.cornerRadius = button.frame.size.height/2.0
            button.clipsToBounds = true
        }
        
        let buttonSet4 = viewButtonSet.subviews.filter{$0 is UIButton}
        for button in buttonSet4 {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        // round button corners
        let buttonSet5 = viewLevelCheckControls.subviews.filter{$0 is UIButton}
        for button in buttonSet5 {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        imageView.layer.cornerRadius = imageView.frame.size.width/2.0
        imageView.layer.borderWidth = 1.0
        imageView.layer.borderColor = (UIColor.gray.cgColor)
        imageView.clipsToBounds = true
        
        imgViewCompositeAngle.layer.cornerRadius = imgViewCompositeAngle.frame.size.width/2.0
        imgViewCompositeAngle.layer.borderWidth = 1.0
        imgViewCompositeAngle.layer.borderColor = (UIColor.green.cgColor)
        imgViewCompositeAngle.clipsToBounds = true
        
        // other UI settings
        buttonShowSamples.isHidden = true
        buttonSaveSamples.isHidden = true
        
        // updating the queue quite fast
        motionManager.deviceMotionUpdateInterval = 0.01
        
        // uses a separate queue, since we have calculations to come
        let queue = OperationQueue()
        
        // motion manager is set to measure wrt True North
        motionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xMagneticNorthZVertical, to:queue) {
            [weak self] (data, error) in
            
            // motion processing here
            guard let data = data, error == nil else {
                return
            }
            
            // attitude is more accurate and combines both gyroscope and accelerometer data
            let attitudePitch = (data.attitude.pitch) * 180 / Double.pi
            let attitudeRoll = (data.attitude.roll) * -180 / Double.pi
            let attitudeYaw: Double = data.attitude.yaw * 180.0 / Double.pi + 450.0
            
            // IIR filter
            self?.filteredPitch = attitudePitch*(1-(self?.alpha)!) + (self?.filteredPitch)!*(self?.alpha)!
            self?.filteredRoll = attitudeRoll*(1-self!.alpha) + (self?.filteredRoll)!*(self?.alpha)!
            //self?.filteredYaw = attitudeYaw*(1-self!.alpha) + (self?.filteredYaw)!*self!.alpha
            
            // Yaw needs smoothing at the 270 degree point
            if abs(attitudeYaw - (self?.filteredYaw)!) < 90.0 {
                self?.filteredYaw = attitudeYaw*(1-self!.alpha) + (self?.filteredYaw)!*self!.alpha
            } else {
                self?.filteredYaw = attitudeYaw
            }
            
            // composite level magnitude
            self!.compositeLevelMagnitude = sqrt(attitudePitch*attitudePitch + attitudeRoll*attitudeRoll)
            
            // composite level angle: handle divide by zero
            if (self!.filteredPitch == 0) {
                if (self!.filteredRoll >= 0) {
                    self!.compositeLevelAngle = 0.0
                } else {
                    self!.compositeLevelAngle = Double.pi
                }
            } else {
                //// take each quadrant separately
                // Top Right
                if (self!.filteredPitch > 0 && self!.filteredRoll >= 0) {
                    self!.compositeLevelAngle = atan(self!.filteredRoll/self!.filteredPitch)
                }
                // Top Left : same as above
                if (self!.filteredPitch > 0 && self!.filteredRoll < 0) {
                    self!.compositeLevelAngle = atan(self!.filteredRoll/self!.filteredPitch)
                }
                // Bottom Right
                if (self!.filteredPitch < 0 && self!.filteredRoll >= 0) {
                    self!.compositeLevelAngle = Double.pi + atan(self!.filteredRoll/self!.filteredPitch)
                }
                //Bottom Left
                if (self!.filteredPitch < 0 && self!.filteredRoll < 0) {
                    self!.compositeLevelAngle = Double.pi + atan(self!.filteredRoll/self!.filteredPitch)
                }
            }
            
            // update level check stats if needed
            if self!.isCheckingLevel {
                // update whatever values larger than before
                if self!.maxSlopeDev < self!.compositeLevelMagnitude {
                    self!.maxSlopeDev = self!.compositeLevelMagnitude
                }
                
                if self!.maxRotationDev < self!.compositeLevelAngle {
                    self!.maxRotationDev = self!.compositeLevelAngle
                }
                
                if fabs(self!.filteredYaw - self!.prevAngle) < 180.0 {
                    self!.rotatedSoFar += fabs(self!.filteredYaw - self!.prevAngle)
                } else {
                    // fine to ignore this sample
                }
                
                // update the previous yaw
                self!.prevAngle = self!.filteredYaw
                
                // update UI
                DispatchQueue.main.async {
                    // update labels
                    self!.labelRotationSoFar.text = String(format: "%.1f", self!.rotatedSoFar)
                    self!.labelPercentDeviation.text = String(format: "Slope deviation: %.1f (tolerance %.1f)", self!.maxSlopeDev, self!.slopeTol)
                    self!.labelDirectionDeviation.text =  String(format: "Rotation deviation: %.1f (tolerance %.1f)", self!.maxRotationDev, self!.rotationTol)
                    
                    // check if we have reached 360 degrees
                    if self!.rotatedSoFar > 360.0 {
                        // stop checking
                        self!.isCheckingLevel = false
                        
                        // compare with tolerances
                        if Double(self!.maxRotationDev) < Double(self!.rotationTol) && Double(self!.maxSlopeDev) < Double(self!.slopeTol) {
                            // phone is level
                            let alertController = UIAlertController(title: "Level Check", message: "The device is now level.", preferredStyle: .alert)
                            let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                self!.cancelLevelCheck(self as Any)
                            }
                            alertController.addAction(actionOK)
                            self!.present(alertController, animated: true, completion: nil)
                        } else {
                            // phone is not level
                            let alertController = UIAlertController(title: "Level Check", message: "The device is not level! Please adjust it and check again.", preferredStyle: .alert)
                            let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                // stay at the level check screen
                            }
                            alertController.addAction(actionOK)
                            
                            // reset level check stats
                            self!.labelRotationSoFar.text = ""
                            self!.maxRotationDev = 0.0
                            self!.maxSlopeDev = 0.0
                            self!.present(alertController, animated: true, completion: nil)
                        }
                    }
                }
            }
            
            DispatchQueue.main.async {
                
                // labels
                self!.labelPitch.text = NSString(format: "%.1f°", self!.filteredPitch) as String
                self!.labelRoll.text = NSString(format: "%.1f°", self!.filteredRoll) as String
                
                // further processing the Yaw value to get rid of the negative values
                
                // TODO: check whether this is really necessaary
                var adjustedYaw: Double = 0
                if self!.filteredYaw > 360 {
                    adjustedYaw = self!.filteredYaw - 360.0
                } else {
                    adjustedYaw = self!.filteredYaw
                }
                
                // assign after adjustment, for record and display
                self?.adjustedTrueHeading = 360 - adjustedYaw
                
                // text labels
                self!.labelYaw.text = NSString(format: "%.1f°", 360 - adjustedYaw) as String
                self!.labelCompositeAngle.text = NSString(format: "%.1f°", abs(self!.compositeLevelMagnitude)) as String
                
                // at this point set the displayed values to strings
                self!.displayedSlopePercent = NSString(format: "%.1f", abs(self!.compositeLevelMagnitude)) as String
                self!.displayedYaw = NSString(format: "%.1f", 360 - adjustedYaw) as String
            
                // sliders
                self!.sliderRoll.value = Float((self?.filteredRoll)!)
                self!.sliderPitch.value = Float((self?.filteredPitch)!)
                
                // update images
                self!.imageView.transform =
                    CGAffineTransform(rotationAngle: CGFloat(adjustedYaw*Double.pi/180.0))
                
                self!.imgViewCompositeAngle.transform =
                    CGAffineTransform(rotationAngle: CGFloat(self!.compositeLevelAngle))
                
                // update level check view, too
                self?.labelDirectionDisplay.text = String(format: "Slope Direction: %.1f°", self!.compositeLevelAngle)
                self?.labelPercentDisplay.text = String(format: "Slope Percent: %.1f %%", self!.compositeLevelMagnitude)
            }
        }

    }
    
    override func viewDidAppear(_ animated: Bool) {
        // hide or show the scanner view as needed
        if isScanning {
            viewScanInProgress.isHidden = false
            //self.buttonSaveAndUpload.blink()
        } else {
            viewScanInProgress.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setters
    func setScanDetails(isReady readyFlag:Bool, scanType typeID:Int, courseName courseStr:String, holeName holeStr: String) {
        // set values
        self.selectedScanType = typeID
        self.courseName = courseStr
        self.holeName = holeStr
        
        // adjust the UI
        self.viewScanInProgress.isHidden = false
        switch self.selectedScanType {
        case 0:
            self.labelScanType.text = "GREEN SCAN"
        case 1:
            self.labelScanType.text = "SURROUND SCAN"
        case 2:
            self.labelScanType.text = "SPRINKLER/DRAIN SCAN"
        default:
            self.labelScanType.text = "OTHER"
        }
        
        self.labelCourseHole.text = String(format: "%@, %@", self.courseName, self.holeName)
        self.labelTypeInProgress.text = String(format: "%@ IN PROGRESS", self.labelScanType.text!)
        
        // set the timestamp
        self.startTimeStamp = Date()
        
        // start recording
        isScanning = true
        isRecording = false
        self.startStopRecording(self)
    }
    
    func resetScanDetails() {
        viewScanInProgress.isHidden = true
        isScanning = false
    }
    
    func clearLog() {
        // clear arrays
        scanLog.removeAllObjects()
        arraySamples.removeAllObjects()
        
        // also remove files in the documents directory, if any
    }
    
    // uploaded the last item
    func markUploadCurrent() {
        let index = scanLog.count - 1
        let lastScan = scanLog[index] as! MappingScan
        lastScan.setUploadInfo(withTimestamp: Date())
    }
    
    // uploaded all items
    func markUploadAll() {
        if scanLog.count > 0 {
            for i in 0...scanLog.count - 1 {
                let lastScan = scanLog[i] as! MappingScan
                lastScan.setUploadInfo(withTimestamp: Date())
            }
        }
    }
    
    //MARK: Button Actions
    @IBAction func startScan(_ sender: Any) {
        selectedScanType = (sender as AnyObject).tag - 200
        self.performSegue(withIdentifier: "segueMainToDetails", sender: self)
    }
    
    @IBAction func saveAndUpload(_ sender: Any) {
        // stop recording
        self.startStopRecording(self)
        
        // saving locally
        self.saveSamplesBeforeUploading(self)
        
        // go to uplad view
        self.performSegue(withIdentifier: "segueMainToUpload", sender: self)
    }
    
    @IBAction func startStopRecording(_ sender: Any) {
        if  self.isRecording {
            // stop
            if recordTimer != nil {
                recordTimer.invalidate()
                recordTimer = nil
            }
            isRecording = false
            // reset button image
            buttonRecord.setImage(UIImage(named: "rec_start.png"), for: UIControl.State.normal)
            
            // can switch to the next view if there are samples
            if samplesLevel.count > 0 {
                buttonShowSamples.isHidden = false
                buttonSaveSamples.isHidden = false
            } else {
                buttonShowSamples.isHidden = true
                buttonSaveSamples.isHidden = true
            }
        } else {
            // start
            samplesPitch.removeAllObjects()
            samplesRoll.removeAllObjects()
            samplesYaw.removeAllObjects()
            samplesLevel.removeAllObjects()
            arraySamples.removeAllObjects()
            isRecording = true
            recordTimestamp = Date()
            recordTimer = Timer.scheduledTimer(timeInterval: self.recordingInterval, target: self, selector: #selector(self.addRecord), userInfo: nil, repeats: true)
            // set button image
            buttonRecord.setImage(UIImage(named: "rec_stop.png"), for: UIControl.State.normal)
            
            // hide button
            buttonShowSamples.isHidden = true
            buttonSaveSamples.isHidden = true
        }
    }
    
    @IBAction func showLevelCheckView(_ sender: Any) {
        viewLevelCheck.isHidden = false
    }
    
    @IBAction func startLevelCheck(_ sender: Any) {
        maxSlopeDev = 0
        maxRotationDev = 0
        rotatedSoFar = 0
        startingAngle = self.filteredYaw
        prevAngle = startingAngle
        isCheckingLevel = true
    }
    
    // close the level check and go back
    @IBAction func cancelLevelCheck(_ sender: Any) {
        self.viewLevelCheck.isHidden = true
        self.startingAngle = 0
        self.prevAngle = 0
        self.maxSlopeDev = 0
        self.maxRotationDev = 0
    }
    
    
    //MARK: File Handling
    @IBAction func saveSamplesBeforeUploading(_ sender: Any) {
        // create scan instance
        self.currentScan = MappingScan.init(withTimeStamp: Date())
        self.currentScan.setScanInfo(withScanType: self.labelScanType.text!, courseName: self.courseName, andHoleName: self.holeName)
        // construct filename
        self.outFileName = self.currentScan.getLocalfilename()
        
        // create the file in the documents directory
        createOrOverwriteEmptyFileInDocuments(filename: self.outFileName)
        if let handle = getHandleForFileInDocuments(filename: self.outFileName ) {
            for (_, sample) in arraySamples.enumerated() {
                let curDataSample = sample as! DataSample
                writeString(string: curDataSample.getCSV(), fileHandle: handle)
            }
        }
        
        // add the data to the scan
        self.currentScan.setSamples(sampleArray: self.arraySamples)
        
        // add the scan to the log
        scanLog.add(self.currentScan)
        
        // DEBUG: open the file for sharing with other apps
        // This allows you to verify the data and also the filename
        /*
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
            debugPrint("ERROR OPENING FILE")
            return
        }
        let documentURL = dir.appendingPathComponent(self.outFileName)
        
        let fileURL = documentURL
        
        let objectsToShare = [fileURL]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        self.present(activityVC, animated: true, completion: nil)
        */
        
        // DONE
    }
    
    // older version, left unchanged for now
    @IBAction func saveSamples(_ sender: Any) {
        // construct filename
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MMM-dd_HH-mm-ss"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        // create file
        let filename = String(format:"%@_PreciseLevel.csv", dateString)
        createOrOverwriteEmptyFileInDocuments(filename: filename)
        if let handle = getHandleForFileInDocuments(filename: filename) {
            for (_, sample) in arraySamples.enumerated() {
                let curDataSample = sample as! DataSample
                writeString(string: curDataSample.getCSV(), fileHandle: handle)
            }
        }
        
        // open the file again for sharing with other apps
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
            debugPrint("ERROR OPENING FILE")
            return
        }
        let documentURL = dir.appendingPathComponent(filename)
        
        let fileURL = documentURL
        
        let objectsToShare = [fileURL]
        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        
        self.present(activityVC, animated: true, completion: nil)
        
        // DONE
    }
    
    
    // MARK: Data Logging
    
    // all values are updated by their respective threads
    // just take them and create a record
    @objc func addRecord() {
        // old stufdf for the table view
        samplesPitch.add(self.filteredPitch)
        samplesRoll.add(self.filteredRoll)
        samplesYaw.add(self.filteredYaw)
        samplesLevel.add(self.compositeLevelMagnitude)
        
        // CSV record
        let record:DataSample = DataSample(withTimeStamp: Date())
        
        // level-related
        var sampleSlope: Double!
        var sampleDirection: Double!
        
        // Slope Percent
        //sampleSlope = 100.0*tan(self.compositeLevelMagnitude.toRadians) // Wikipedia: was wrong
        sampleSlope = self.compositeLevelMagnitude
        
        //sampleDirection = (self.compositeLevelAngle).toDegrees
        // revise by adding the true heading
        sampleDirection = (self.compositeLevelAngle).toDegrees + Double(self.displayedYaw)!
        if sampleDirection > 360.0 {
            sampleDirection -= 360.0
        }
        
        //record.setSlope(with_SlopePercent: sampleSlope, slopeAngle: sampleDirection, andHeading: self.adjustedTrueHeading)
        record.setSlope(with_SlopePercent: sampleSlope, slopeAngle: sampleDirection, andHeading: self.displayedYaw)
        
        // location-related
        record.setLoc(withLocLattitude: self.locLatitude, locLongitude: self.locLongitude, locAccuracy: self.locAccuracy, andMagHeading: self.magHeading)
        
        // add to array
        self.arraySamples.add(record)
        
        // DEBUG: this prints the current sample in CSV format
        //print(record.getCSV())
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // details for a new scan
        if segue.identifier == "segueMainToDetails" {
            // set the type of scan here
            let destController = segue.destination as! ScanDetailsViewController
            destController.setScanType(scanType: selectedScanType)
        }
        
        // upload the current scan
        if segue.identifier == "segueMainToUpload" {
            // set the type of scan here
            let destController = segue.destination as! DataSavedViewController
            // set data to be uploaded
            destController.setMappingData(scanData: self.currentScan)
        }
        
        // show log
        if segue.identifier == "segueMainToLog" {
            // setting locally
            let desctController = segue.destination as! LogViewController
            desctController.setScanLog(log: scanLog)
            
            // also save to user defaults
            /*
            let appSettings = UserDefaults.standard
            appSettings.set(scanLog, forKey: "scanLog")
            appSettings.synchronize()
            */
        }
    }

    @IBAction func unwindToMain(segue:UIStoryboardSegue){
        // Empty unwind segue
    }
    
    //// - no need to edit below this points -
    
    //MARK: Location
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        if (newHeading.headingAccuracy > 0) {
            self.magHeading = newHeading.magneticHeading
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let newLocation = locations[0]
        if (newLocation.horizontalAccuracy > 0) {
            self.locLatitude = newLocation.coordinate.latitude
            self.locLongitude = newLocation.coordinate.longitude
            self.locAccuracy = newLocation.horizontalAccuracy
            
            // also update accuracy on the UI
            labelGPSAccuracy.text = String(format: "%d%", self.locAccuracy)
        }
    }
    
    //MARK: Documents Directory
    func createOrOverwriteEmptyFileInDocuments(filename: String){
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            debugPrint("ERROR IN createOrOverwriteEmptyFileInDocuments")
            return
        }
        let fileURL = dir.appendingPathComponent(filename)
        do {
            try "".write(to: fileURL, atomically: true, encoding: .utf8)
        }
        catch {
            debugPrint("ERROR WRITING STRING: " + error.localizedDescription)
        }
        debugPrint("FILE CREATED: " + fileURL.absoluteString)
    }
    
    
    private func writeString(string: String, fileHandle: FileHandle){
        let data = string.data(using: String.Encoding.utf8)
        guard let dataU = data else {
            debugPrint("ERROR WRITING STRING: " + string)
            return
        }
        fileHandle.seekToEndOfFile()
        fileHandle.write(dataU)
    }
    
    private func getHandleForFileInDocuments(filename: String)->FileHandle?{
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else {
            debugPrint("ERROR OPENING FILE")
            return nil
        }
        let fileURL = dir.appendingPathComponent(filename)
        do {
            let fileHandle: FileHandle? = try FileHandle(forWritingTo: fileURL)
            return fileHandle
        }
        catch {
            debugPrint("ERROR OPENING FILE: " + error.localizedDescription)
            return nil
        }
    }
}

//MARK: Angle Calculations
extension Double {
    var toRadians: Double { return self * .pi / 180.0 }
    var toDegrees: Double { return self * 180.0 / .pi }
}

// MARK: Blinking button
extension UIView{
    func blink() {
        self.alpha = 1.0
        UIView.animate(withDuration: 0.5, delay: 0.0, options: [.curveLinear, .repeat], animations: {self.alpha = 0.0}, completion: nil)
    }
}
