//
//  UploadViewController.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/04/07.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation
import UIKit

class UploadViewController: UIViewController
{
    // data to be uploaded are here.
    // Please see MappingScan.swift for details on data.
    var mappingData:NSMutableArray!
    
    var uploadSucceeded: Bool = false
    
    // UI
    @IBOutlet weak var buttonUploadProgress: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // round button corners
        let buttonSet = view.subviews.filter{$0 is UIButton}
        for button in buttonSet {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }

        
        // data upload now happens at viewDidAppear
        
        // done
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // check for Internet connectivity
        if Reachability.isConnectedToNetwork(){
            
            // start uploading data on a thread
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else {
                    return
                }
                // perform data uplaod
                self.uploadData()
                
                // update UI when done
                DispatchQueue.main.async { [weak self] in
                    // just the button color anad text
                    self!.buttonUploadProgress.backgroundColor = #colorLiteral(red: 0.0, green: 1.0, blue: 0.0, alpha: 1.0)
                    self!.buttonUploadProgress.setTitle("COMPLETE", for: .normal)
                    
                    // mark upload for future processing
                    self!.uploadSucceeded = true
                    
                    // show alert and go back
                    let alertController = UIAlertController(title: "Upload Complete", message: "Uploaded all scans", preferredStyle: .alert)
                    let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        // go back
                        self!.performSegue(withIdentifier: "segueUploadToMain", sender: self)
                    }
                    alertController.addAction(actionOK)
                    self!.present(alertController, animated: true, completion: nil)
                }
            }
            
        }else{
            print("Internet Connection not Available!")
            // show alert and go back
            let alertController = UIAlertController(title: "No Connection", message: "The device is not connected to the Internet. The scan has been saved locally.", preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                // go back
                self.performSegue(withIdentifier: "segueUploadToMain", sender: self)
            }
            alertController.addAction(actionOK)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.performSegue(withIdentifier: "segueUploadToMain", sender: self)
    }
    
    //MARK: Setter
    func setMappingData(scanData data:NSMutableArray)
    {
        self.mappingData = data
    }
    
    //MARK: Data Upload
    func uploadData()
    {
        // Code to upload data should be here
        // you can loop through "mappingData" and upload individual scans that were not previously uploaded
        // getters for accessing the data have been declared in MappingScan.swift
        sleep(3)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "segueUploadToMain" {
            // mark the log as uploaded if upload was successful
            if uploadSucceeded {
                let destController = segue.destination as! ViewController
                destController.markUploadAll()
            }
        }
    }
    
}
