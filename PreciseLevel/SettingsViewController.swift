//
//  SettingsViewController.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/04/06.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation
import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate {
    
    // inputs
    var name:String!
    var emailAddress:String!
    var phoneNum:String!
    var address:String!
    var venmoAcct:String!
    var paypalAcct:String!
    var rotationTol:String!
    var slopeTol:String!
    
    // keypad manipulation
    var keypadShown:Bool!
    var kKeypadHeight:Int = 180 // just to explain why it is in the code
    var kMovableTagmin:Int = 100
    
    // UI controls
    @IBOutlet weak var txtFieldName: UITextField!
    @IBOutlet weak var txtFieldEmail: UITextField!
    @IBOutlet weak var txtFieldPhone: UITextField!
    @IBOutlet weak var txtViewAddress: UITextView!
    @IBOutlet weak var txtfieldVenmo: UITextField!
    @IBOutlet weak var txtFieldPaypal: UITextField!
    @IBOutlet weak var txtFieldRotation: UITextField!
    @IBOutlet weak var txtFieldSlope: UITextField!
    @IBOutlet weak var buttonSave: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load previously saved setttings, if any
        name = UserDefaults.standard.string(forKey: "name")
        emailAddress = UserDefaults.standard.string(forKey: "emailAddress")
        phoneNum = UserDefaults.standard.string(forKey: "phoneNum")
        address = UserDefaults.standard.string(forKey: "address")
        venmoAcct = UserDefaults.standard.string(forKey: "venmoAcct")
        paypalAcct = UserDefaults.standard.string(forKey: "paypalAcct")
        rotationTol = UserDefaults.standard.string(forKey: "rotationTol")
        slopeTol = UserDefaults.standard.string(forKey: "slopeTol")
        
        // update the UI
        txtFieldName.text = name
        txtFieldEmail.text = emailAddress
        txtFieldPhone.text = phoneNum
        txtViewAddress.text = address
        txtfieldVenmo.text = venmoAcct
        txtFieldPaypal.text = paypalAcct
        txtFieldRotation.text = rotationTol
        txtFieldSlope.text = slopeTol
        keypadShown = false
        
        // round button corners
        let buttonSet = view.subviews.filter{$0 is UIButton}
        for button in buttonSet {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        // done
    }
    
    @IBAction func saveSettings(_ sender: Any) {
        // extract values from the UI
        name = txtFieldName.text
        emailAddress = txtFieldEmail.text
        phoneNum = txtFieldPhone.text
        address = txtViewAddress.text
        venmoAcct = txtfieldVenmo.text
        paypalAcct = txtFieldPaypal.text
        
        // save current settings
        UserDefaults.standard.set(name, forKey: "name")
        UserDefaults.standard.set(emailAddress, forKey: "emailAddress")
        UserDefaults.standard.set(phoneNum, forKey: "phoneNum")
        UserDefaults.standard.set(address, forKey: "address")
        UserDefaults.standard.set(venmoAcct, forKey: "venmoAcct")
        UserDefaults.standard.set(paypalAcct, forKey: "paypalAcct")
        UserDefaults.standard.set(rotationTol, forKey: "rotationTol")
        UserDefaults.standard.set(slopeTol, forKey: "slopeTol")
        UserDefaults.standard.synchronize()
        
        // show alert and go back
        let alertController = UIAlertController(title: "Saved", message: "Saved the updated settings.", preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            // go back
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(actionOK)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Text Field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.tag >= kMovableTagmin {
            self.view.frame.origin.y -= 180
            self.keypadShown = true
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        if  self.keypadShown {
            self.view.frame.origin.y += 180
            keypadShown = false
        }
        return false
    }
    
    //MARK: Text View
    func textViewDidBeginEditing(_ textView: UITextView) {
        self.view.frame.origin.y -= 180
        self.keypadShown = true
    }
    
    @IBAction func closeMemoKeypad(_ sender: Any) {
        txtViewAddress.resignFirstResponder()
        if  keypadShown {
            self.view.frame.origin.y += 180
            keypadShown = false
            
            // as requested, save the information and go back.
            // disable later if needed.
            self.saveSettings(self)
        }
    }
}
