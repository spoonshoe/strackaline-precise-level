//
//  DataSample.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/03/23.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation

class DataSample: NSObject {
    var timestamp: Date!
    var slopePercent: Double!
    var slopeAngle: Double!
    var trueHeading: String!
    var locLatitude: Double!
    var locLongitude: Double!
    var locAccuracy: Double!
    var magneticHeading: Double!
    
    // constructor
    init(withTimeStamp sampleDate:Date) {
        super.init()
        self.timestamp = sampleDate
    }
    
    // set slope attributes
    func setSlope(with_SlopePercent percent: Double, slopeAngle angle: Double, andHeading heading: String ) {
        self.slopePercent = percent
        self.slopeAngle = angle
        self.trueHeading = heading
    }
    
    // set location attributes
    func setLoc(withLocLattitude latitude: Double, locLongitude longitude: Double, locAccuracy accuracy: Double,andMagHeading heading: Double) {
        self.locLatitude = latitude
        self.locLongitude = longitude
        self.locAccuracy = accuracy
        self.magneticHeading = heading
    }
    
    // get string with commas for a CSV file
    func getCSV() -> String {
        // prepare timestamp
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MMM-dd, HH:mm:ss"
        let date = self.timestamp
        let dateString = dateFormatter.string(from: date!)
        // ready to create string
        var result: String!
        result = String(format: "%@, %.1f, %.1f, %@, %.1f, %.10f, %.10f, %.2f\n", dateString, self.slopePercent, self.slopeAngle, self.trueHeading, self.magneticHeading, self.locLatitude, self.locLongitude, self.locAccuracy)
            print(result)
        return result
    }
}
