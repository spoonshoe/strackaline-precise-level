//
//  LogViewController.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/04/07.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation
import UIKit

class LogViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // log of scans
    var scanLog:NSMutableArray = NSMutableArray.init()
    var isCleared: Bool = false
    
    @IBOutlet weak var tableViewScanLog: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // round button corners
        let buttonSet = view.subviews.filter{$0 is UIButton}
        for button in buttonSet {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        // load data, and we aredone
        tableViewScanLog.reloadData()
    }
    
    @IBAction func goBack(_ sender: Any) {
        // do I need to save?
        
        // just close
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clearLog(_ sender: Any) {
        // get the user to confirm
        let alertController = UIAlertController(title: "Are You Sure?", message: "All saved logs will be removed!", preferredStyle: .alert)
        
        let action2 = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction) in
            return
        }
        
        let action3 = UIAlertAction(title: "Delete", style: .destructive) { (action:UIAlertAction) in
            // indicate deletion
            self.isCleared = true
            
            // perform unwind
            self.performSegue(withIdentifier: "segueLogToMain", sender: self)
        }
        
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    // Setters
    func setScanLog(log:NSMutableArray) {
        self.scanLog = log
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scanLog.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // create a new cell if needed or reuse an old one
        let cell:UITableViewCell = self.tableViewScanLog.dequeueReusableCell(withIdentifier: "cellLogEntry") as UITableViewCell!
        
        var labelCreationTimeStamp:UILabel!
        var labelCourseHole:UILabel!
        var labelUploadTimeStamp:UILabel!
        
        labelCreationTimeStamp = (cell.viewWithTag(100) as! UILabel)
        labelCourseHole = (cell.viewWithTag(101) as! UILabel)
        labelUploadTimeStamp = (cell.viewWithTag(102) as! UILabel)
        let labelScanType = (cell.viewWithTag(103) as! UILabel)
        
        // set the text from the data
        let currentScan = scanLog[indexPath.row] as! MappingScan
        labelCourseHole.text = String(format: "%@, %@", currentScan.getCourseName(), currentScan.getHoleName())
        // dates
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
        labelCreationTimeStamp.text = String(format: "Created: %@", dateFormatter.string(from: currentScan.getCreationTimestamp()) )
        if currentScan.hasBeenUploaded() {
            labelUploadTimeStamp.text = String(format: "Uploaded: %@", dateFormatter.string(from: currentScan.getUploadTimestamp()) )
        } else {
            labelUploadTimeStamp.text = "Not uploaded yet."
        }
        
        // scan type
        labelScanType.text = currentScan.getScanType()
        
        // done
        return cell
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // we only have one segue anyway
        let destController = segue.destination as! ViewController
        if isCleared {
            destController.clearLog()
        }
    }
    
    
    // end
}
