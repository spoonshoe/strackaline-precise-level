//
//  DataSavedViewController.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/04/09.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation
import UIKit


class DataSavedViewController: UIViewController {
    // data to be uploaded
    var scanData:MappingScan!
    var uploadURL : String = "http://www.aventinestaging.com/greendatacollector/upload.php"
    
    // was upload successful?
    var uploadSucceeded: Bool = false
    
    @IBOutlet weak var buttonDataSaved: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // round button corners
        let buttonSet = view.subviews.filter{$0 is UIButton}
        for button in buttonSet {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        // done
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        buttonDataSaved.isHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool)
    {
        super .viewDidAppear(animated)
        if Reachability.isConnectedToNetwork(){
            buttonDataSaved.isHidden = false
            
            // upload the data
            self.uploadData()
            
            sleep(2)
            
            // mark upload for future processing
            uploadSucceeded = true
            
            // show alert and go back
            let alertController = UIAlertController(title: "Upload Complete", message: "Uploaded this scan", preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                // go back
                self.performSegue(withIdentifier: "segueSaveToMain", sender: self)
            }
            alertController.addAction(actionOK)
            self.present(alertController, animated: true, completion: nil)
            
        }else{
            buttonDataSaved.isHidden = true
            print("Internet Connection not Available!")
            // show alert and go back
            let alertController = UIAlertController(title: "No Connection", message: "The device is not connected to the Internet. The scan is saved locally.", preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                // go back
                self.performSegue(withIdentifier: "segueSaveToMain", sender: self)
            }
            alertController.addAction(actionOK)
            self.present(alertController, animated: true, completion: nil)
        }
        
        // done
    }
    
    func uploadData()
    {
         guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
         debugPrint("ERROR OPENING FILE")
         return
         }
         
         let documentURL = dir.appendingPathComponent(self.scanData.getCSVFileName())
         let fileURL = documentURL
//         let objectsToShare = [fileURL]
//         let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//         self.present(activityVC, animated: true, completion: nil)
        
        let fileData = try! Data(contentsOf: fileURL)
        
        
        // done
        let url = URL(string: uploadURL)
        let boundary = UUID().uuidString
        let session = URLSession.shared
        var urlRequest = URLRequest(url: url!)
        urlRequest.httpMethod = "POST"
        urlRequest.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        
       // let dataToSend    = NSData(contentsOfFile: fileURL)!
        
        var data = Data()
        data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
        data.append("Content-Disposition: form-data; name=\"file\"; filename=\"\(self.scanData.getCSVFileName())\"\r\n".data(using: .utf8)!)
        data.append("Content-Type: image/png\r\n\r\n".data(using: .utf8)!)
        data.append(fileData)
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        session.uploadTask(with: urlRequest, from: data, completionHandler: { responseData, response, error in
            if error == nil
            {
                    let alertController = UIAlertController(title: "Upload Complete", message: "Uploaded this scan", preferredStyle: .alert)
                    let actionOK = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        // go back
                        self.performSegue(withIdentifier: "segueSaveToMain", sender: self)
                    }
                    alertController.addAction(actionOK)
                    self.present(alertController, animated: true, completion: nil)
            }
            else
            {
               print("error")
            }
        }).resume()

        
    }
    
    //MARK: Setter
    func setMappingData(scanData data:MappingScan)
    {
        self.scanData = data
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.identifier == "segueSaveToMain" {
            // reset main screen
            let destinationVC = segue.destination as! ViewController
            destinationVC.resetScanDetails()
            if uploadSucceeded {
                destinationVC.markUploadCurrent()
            }
        }
    }
}
