//
//  File.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/03/21.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import UIKit
import Foundation

class SamplesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    //MARK: TableView Delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return samplesLevel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSample")!
        let curLevel:Double = samplesLevel.object(at: indexPath.row) as! Double
        let curHeading = samplesDirection[indexPath.row] as! Double
        cell.textLabel?.text = String(format: "%.1f, %.1f", curLevel, curHeading)
        return cell
    }
    
    var sampleDate: Date!
    var samplesLevel: NSMutableArray = NSMutableArray.init()
    var samplesDirection: NSMutableArray = NSMutableArray.init()
   
    @IBOutlet weak var labelSampleStartTime: UILabel!
    @IBOutlet weak var tableViewSamples: UITableView!
    
    //MARK: Set Data
    func setSamples(timestamp:Date, arrayLevel:NSMutableArray, arrayDirection:NSMutableArray)
    {
        sampleDate = timestamp
        samplesLevel = arrayLevel.mutableCopy() as! NSMutableArray
        samplesDirection = arrayDirection.mutableCopy() as! NSMutableArray
        // done
        print("%d", samplesLevel.count)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // show date
        let dateformatter = DateFormatter()
        dateformatter.dateStyle = DateFormatter.Style.medium
        dateformatter.timeStyle = DateFormatter.Style.medium
        labelSampleStartTime.text = dateformatter.string(from: sampleDate)
        
        // show samples
        tableViewSamples.reloadData()
    }
    
    //MSRK: Button Actions
    @IBAction func goBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
