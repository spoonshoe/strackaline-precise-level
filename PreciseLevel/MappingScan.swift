//
//  MappingScan.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/04/12.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation

class MappingScan:NSObject {
    // properties
    var timestamp:Date!
    var scanType:String!
    var courseName:String!
    var holeName:String!
    var sampleData:NSMutableArray=NSMutableArray.init()
    var timeUploaded:Date!
    var fileName:String!
    
    // constructor
    init(withTimeStamp startDate:Date) {
        super.init()
        self.timestamp = startDate
    }
    
    // set scan attributes
    func setScanInfo(withScanType type: String, courseName course: String, andHoleName hole: String ) {
        self.scanType = type
        self.courseName = course
        self.holeName = hole
    }
    
    // creates a local file name for saving data within the app.
    func getLocalfilename()-> String {
        // construct anad return filename
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MMM-dd_HH-mm-ss"
        let date = self.timestamp as Date
        let dateString = dateFormatter.string(from: date)
        
        // create file name using the timestamp, course name and hole name and scan type
        self.fileName = String(format:"%@_%@_%@_%_Scan.csv", dateString, self.courseName, self.holeName, self.scanType)
        
        // done
        return self.fileName
    }
    
    // set the sampled data for storing locally
    func  setSamples(sampleArray data:NSMutableArray) {
        self.sampleData = data.mutableCopy() as! NSMutableArray
    }
    
    // Please use this to set upload time after you upload the data
    func setUploadInfo(withTimestamp uploaded:Date) {
        self.timeUploaded = uploaded
    }
    
    //MARK: Getters for uploading purposes
    func getCreationTimestamp()->Date {
        return timestamp
    }
    
    func getScanType()->String {
        return scanType
    }
    
    func getCourseName()->String {
        return courseName
    }
    
    func getHoleName()->String {
        return holeName
    }
    
    func hasBeenUploaded()->Bool {
        if timeUploaded != nil {
            return true
        }
        else {
            return false
        }
    }
    
    func getUploadTimestamp()->Date {
        return timeUploaded
    }
    
    func getCSVFileName()->String {
        return fileName
    }
    
    func getSampleData()->NSMutableArray {
        return sampleData
    }
    
    // end
}
