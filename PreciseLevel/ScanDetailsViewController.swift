//
//  ScanDetailsViewController.swift
//  PreciseLevel
//
//  Created by Chamin Morikawa on 2019/04/07.
//  Copyright © 2019 Chamin Morikawa. All rights reserved.
//

import Foundation
import UIKit

class ScanDetailsViewController: UIViewController, UITextFieldDelegate {
    var scanType:Int = -1
    var courseName:String!
    var holeName:String!
    
    // flag to indicate whether we are ready to scan
    var readyToScan:Bool=false
    
    @IBOutlet weak var labelScanType: UILabel!
    @IBOutlet weak var txtFieldCourse: UITextField!
    @IBOutlet weak var txtFieldHole: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if there are saved names, load them
        let appSettings = UserDefaults.standard
        let savedCourseName = appSettings.string(forKey: "courseName")
        if savedCourseName != nil {
            courseName = savedCourseName
            txtFieldCourse.text = courseName
        }
        let savedHoleName = appSettings.string(forKey: "holeName")
        if savedHoleName != nil {
            holeName = savedHoleName
            txtFieldHole.text = holeName
        }
        
        switch self.scanType {
        case 0:
            labelScanType.text = "GREEN SCAN"
            break
        case 1:
            labelScanType.text = "SURROUND SCAN"
            break
        case 2:
            labelScanType.text = "SPRINKLER/DRAIN"
            break
        default:
            // just don't set a scan type
            break
        }
        
        // round button corners
        let buttonSet = view.subviews.filter{$0 is UIButton}
        for button in buttonSet {
            button.layer.cornerRadius = 8
            button.clipsToBounds = true
        }
        
        // done
    }
    
    //MARK: Setter
    func setScanType(scanType typeID:Int) {
        self.scanType = typeID
    }
    
    //MARK: Buttons
    @IBAction func goBack(_ sender: Any) {
        self.readyToScan = false
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func goBackAndScan(_ sender: Any) {
        // go back
        self.readyToScan = true
        self.performSegue(withIdentifier: "segueDetailsToMain", sender: self)
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        courseName = txtFieldCourse.text
        holeName = txtFieldHole.text
        
        // save the names first
        let appSettings = UserDefaults.standard
        appSettings.setValue(courseName, forKey: "courseName")
        appSettings.setValue(holeName, forKey: "holeName")
        appSettings.synchronize()
        
        // update main class
        if segue.identifier == "segueDetailsToMain" {
            let destinationVC = segue.destination as! ViewController
            destinationVC.setScanDetails(isReady: self.readyToScan,scanType: self.scanType, courseName: self.courseName, holeName: self.holeName)
        }
    }
    

    //MARK: Text Field
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}

